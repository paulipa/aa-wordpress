from django.apps import AppConfig


class WordpressConfig(AppConfig):
    name = "wordpress"
    label = "wordpress"
    verbose_name = "Alliance Auth Wordpress Service"
